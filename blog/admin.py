from django.contrib import admin
from blog.models import CategoryModel
from blog.models import PostModel
from blog.models import CommentModel
from blog.models import ContactModel

@admin.register(CategoryModel)
class CategoriesAdmin(admin.ModelAdmin):
    search_fields = ('name', 'slug')
    list_display = (
        'name', 'slug'
    )

@admin.register(PostModel)
class PostsAdmin(admin.ModelAdmin):
    search_fields = ('title', 'content')
    list_display = (
        'title', 'created_at', 'updated_at'
    )

@admin.register(CommentModel)
class CommentAdmin(admin.ModelAdmin):
    search_fields = ('author__username', 'post__title')
    list_display = (
        'author', 'post', 'content', 'created_at', 'updated_at'
    )

@admin.register(ContactModel)
class ContactAdmin(admin.ModelAdmin):
    search_fields = ('email', 'name_surname')
    list_display = (
        'email', 'name_surname', 'message', 'created_at'
    )
