from django.db import models
from django.contrib.auth.models import User
from blog.models import PostModel
from blog.abstract_models import DateAbstractModel

class CommentModel(DateAbstractModel):
    author = models.ForeignKey('account.CustomUserModel', on_delete=models.CASCADE, related_name='comment')
    post = models.ForeignKey(PostModel, on_delete=models.CASCADE, related_name='comments')
    content = models.TextField()
    class Meta:
        db_table = 'comment'
        verbose_name_plural = 'Comments'
        verbose_name = 'Comment'

    def __str__(self):
        return self.content