from django.db import models
from autoslug import AutoSlugField
from blog.models import CategoryModel
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from blog.abstract_models import DateAbstractModel

class PostModel(DateAbstractModel):
    image = models.ImageField(upload_to='post_images')
    title = models.CharField(max_length=50)
    content = RichTextField()
    slug = AutoSlugField(populate_from='title', unique=True)
    categories = models.ManyToManyField(CategoryModel, related_name='post')
    author = models.ForeignKey('account.CustomUserModel', on_delete=models.CASCADE, related_name='post')

    class Meta:
        db_table = 'post'
        verbose_name_plural = 'Posts'
        verbose_name = 'Post'

    def __str__(self):
        return self.title